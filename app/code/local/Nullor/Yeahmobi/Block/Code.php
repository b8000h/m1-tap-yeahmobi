<?php
/**
 * @descripion  Code Block
 * Facebook 官方的做法是分拆为若干 Block，其中一个为 common 为其余所继承，一个 Block 负责一个 phtml
 * 这里涉及到 层 的增加的必要性
 * 像这种独立的 Block 的内容很少，全部放同一个内也是不打紧的
 */
class Nullor_Yeahmobi_Block_Code extends Mage_Core_Block_Template {

	/**
	 * Renders pixel code if module is enabled
	 * @return [type]
	 */
	public function _toHtml()
    {
        if (Mage::helper('nullor_yeahmobi')->isEnabled()){
            return parent::_toHtml();
        }
    }

    /**
     * 从后台(即 core_config_data )获取设置好的 Pixel Id
     * @return [type]
     */
    public function getPixelId(){
    	return Mage::helper('nullor_yeahmobi')->getPixelId();
    }

    /**
     * section 在此处指具体的 controller action，大可理解做具体页面
     * 是重要的判断点，如此即可避免在 layout.xml 中找 handle
     * @return [type]
     */
    private function _getSection(){
        $pageSection  = Mage::app()->getFrontController()->getAction()->getFullActionName(); 
        return  $pageSection; 
    }


    /**************************************************************************/


    /**
     * Get current store currency
     * @return [type]
     */
    private function _getStoreCurrency(){
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }
    /**
     * 同上，哪个名字好些待决定？
     */
    public function getCurrency() {
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }

    /**
     * 也就是还有 category，但似乎没用上
     */
    public function getContentType() {
        return 'product';
    }

    /**
     * 将 array 转成适用于参数 content_ids 的需要
     */
    public function arryToContentIdString($a) {
        return implode(',', array_map(function ($i) { return '"'.$i.'"'; }, $a));
    }

    public function escapeQuotes($string) {
        return addslashes($string);
    }


    /**
     * 未登录用户则
     * 已登录用户则返回个唯一的 id
     */
    public function pixelInitCode() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn() ) {
        return "var uid=false";
        } else {
        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        return "var uid=" . md5($customer_id);
        }
    }

    /**************************************************************************/


    /**
     * 此处 view 指的就是 product view 页
     * 无需用到 event
     * 所需要的通过一个物 Mage::registry('current_product') 就能获取完
     * @return [type]
     */
    public function getViewContentEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->viewContentEnabled()){ 
            if($pageSection == 'catalog_product_view'){
                /*
                return "mkq('track', 'ViewContent');";
                */
                $extra = $this->getViewContent();
                $extra = json_encode($extra);
                return "mkq('track', 'ViewContent', ". $extra .");";
            }

        }
    }

    protected function getViewContent()
    {
        $extra = array(
            'content_type' => $this->getContentType(),
            'content_ids' => '['. $this->getContentIDs() . ']',
            'content_name' => $this->getContentName(),
            'content_category' => $this->getContentCategory(),
            'value' => $this->getValue(),
            'currency' => $this->getCurrency()
        );
        return $extra;
    }

    // 这是应对 product list 的
    public function getContentIDs() {
        $products = array();
        $products[] = Mage::registry('current_product')->getId();
        return $this->arryToContentIdString($products);
    }

    public function getContentName() {
        return $this->escapeQuotes(Mage::registry('current_product')->getName());
    }

    /**
     * 此处经常有问题
     */
    public function getContentCategory() {
        return Mage::registry('current_product')->getCategory() ?
        $this->escapeQuotes(
            Mage::registry('current_product')->getCategory()->getName()) : '';
    }

    public function getValue() {
        //return Mage::registry('current_product')->getPrice(); // Facebook 官方此处对 Price 的理解有误，做了错误的使用
        return Mage::registry('current_product')->getFinalPrice();
    }

    /**************************************************************************/


    /**
     * Return Search event track
     * @return [type]
     */
    public function getSearchEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->searchEnabled()){
            if($pageSection == 'catalogsearch_result_index' || $pageSection == 'catalogsearch_advanced_result'){ 
                //return "mkq('track', 'Search');";
                $extra = $this->getSearch();
                $extra = json_encode($extra);
                return "mkq('track, 'Search', ". $extra . ");";
            }
        }
    }

    protected function getSearch()
    {
        $extra = array(
            'query' => $this->getSearchQuery()
        );
        return $extra;
    }


    public function getSearchQuery() {
        return htmlspecialchars(
            $this->getRequest()->getParam('q'),
            ENT_QUOTES,
            'UTF-8');
    }


    /**************************************************************************/


    /**
     * Return AddToCart event track
     * @return [type]
     */
    public function getAddToCartEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->addToCartEnabled()){

            $pixelEvent = Mage::getModel('core/session')->getYeahmobiAddToCart();
            if($pixelEvent){
                //Unset event
                Mage::getModel('core/session')->unsYeahmobiAddToCart();
                /*
                return "mkq('track', 'AddToCart');";   
                */

                $extra = Mage::getModel('core/session')->getData('yeahmobi_addtocart');
                Mage::getModel('core/session')->unsetData('yeahmobi_addtocart');
                // 亦可在本物中获取
                // $extra = $this->getAddToCart();

                $extra = json_encode($extra);
                return "mkq('track', 'AddToCart', " . $extra . ");";
            }            
        }
    }

    /**
     * 因为 AddToCart 可以发生在 product view，亦可以发生在 product list
     * 似乎没有覆盖到 product list 的
     * 其实，AddToCart 及 AddToWishlist 是没有页面的更新的，须通过 event 
     * 这就是为什么 Facebook 官方插件 此处
     */
    protected function getAddToCart()
    {
        $extra = array(
            'content_type' => $this->getContentType(),
            'content_ids' => '['. $this->getAddToCartContentIDs() . ']',
            'content_name' => $this->getContentName(),
            'content_category' => $this->getContentCategory(),
            'value' => $this->getValue(),
            'currency' => $this->getCurrency()
        );
        return $extra;
    }


    private $addToCartArray;

    public function shouldFireAddToCart() {
        $a = $this->getAddToCartArray();
        return is_array($a) && count($a) > 0;
    }

    public function getAddToCartContentIDs() {
        $products = $this->getAddToCartArray();
        $this->clearAddToCartArray();
        return $this->arryToContentIdString($products);
    }

    private function getAddToCartArray() {
        if ($this->addToCartArray) {
            return $this->addToCartArray;
        } else {
        $session = Mage::getSingleton("core/session", array("name"=>"frontend"));
        $this->addToCartArray = $session->getData("fbms_add_to_cart") ?: array();
            return $this->addToCartArray;
        }
    }

    private function clearAddToCartArray() {
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
        $session->setData("fbms_add_to_cart", array());
    }


    /**************************************************************************/


    /**
     * Return AddToWishlist event track
     * @return [type]
     */
    public function getAddToWishlistEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->addToWhishlistEnabled()){            
            $pixelEvent = Mage::getModel('core/session')->getYeahmobiAddToWishlist();
            if($pixelEvent){
                //Unset event
                Mage::getModel('core/session')->unsYeahmobiAddToWishlist();

                $extra = Mage::getModel('core/session')->getData('yeahmobi_addtowishlist');
                Mage::getSingleton('core/session')->unsetData('yeahmobi_addtowishlist');

                $extra = json_encode($extra);
                return "mkq('track', 'AddToWishlist', ". $extra . ");";   
            }
        }
    }


    /**************************************************************************/


    /**
     * Return InitiateCheckout event track
     * @return [type]
     */
    public function getInitiateCheckoutEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->initiateCheckoutEnabled()){
            if ($pageSection == 'checkout_onepage_index' || $pageSection == 'onestepcheckout_index_index' || $pageSection == 'opc_index_index'){
                //return "mkq('track', 'InitiateCheckout');";

                /*
                $extra = Mage::getModel('core/session')->getData('fb_initiatecheckout');
                Mage::getSingleton('core/session')->unsetData('fb_initiatecheckout');

                $extra = json_encode($extra);
                return "mkq('track', 'InitiateCheckout', ". $extra . ");";  
                */

                $quote = Mage::getSingleton('checkout/session')->getQuote();
                $numItems = $quote->getAllVisibleItems();

                $itemSkus = array();
                foreach ($numItems as $item) {
                    $itemSkus[] = htmlspecialchars($item->getSku());
                }

                $initiateCheckoutData = array();
                $initiateCheckoutData['value'] = number_format($quote->getSubtotal(), 2);
                $initiateCheckoutData['num_items'] = count($numItems);
                $initiateCheckoutData['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();
                $initiateCheckoutData['content_type'] = 'product';
                $initiateCheckoutData['content_ids'] = $itemSkus;

                $extra = json_encode($initiateCheckoutData);
                return "mkq('track', 'InitiateCheckout', ". $extra . ");";  
            }
        }
    }

    protected function getInitiateCheckout()
    {
        $extra = array(
            'content_type' => $this->getContentType(),
            'content_ids' => '['. $this->getInitiateCheckoutContentIDs() . ']',
            'content_name' => $this->getContentName(),
            'value' => $this->getInitiateCheckoutValue(),
            'currency' => $this->getCurrency()
        );
        return $extra;
    }

    public function getInitiateCheckoutContentIDs() {
        $products = array();
        $items =
        Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems(); // quote 业已在 session 中
        foreach ($items as $item) {
            $products[] = $item->getProductId();
        }
        return $this->arryToContentIdString($products);
    }

    public function getInitiateCheckoutValue() {
        $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals();
        return $totals['grand_total']->getValue() ?: 0;
    }


    /**************************************************************************/



    /**
     * Return AddPaymentInfo event track
     * @return [type]
     */
    public function getAddPaymentInfoEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->addPaymentInfoEnabled()){
            $pixelEvent = Mage::getModel('core/session')->getYeahmobiPaymentInfo();
            if($pixelEvent){
                //Unset event
                Mage::getModel('core/session')->unsYeahmobiPaymentInfo();

                return "mkq('track', 'AddPaymentInfo');";
            }
        }
    }


    /**************************************************************************/


    /**
     * Return Purchase event track
     * @return [type]
     */
    public function getPurchaseEvent(){
        $pageSection        = $this->_getSection();
        $currentCurrency    = $this->_getStoreCurrency();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->purchaseEnabled()){
        
            $pixelEvent = Mage::getModel('core/session')->getYeahmobiPurchase();
            if($pixelEvent){
                // Mage::log('RemmotePixel - pageSection: '. $pageSection);

                // Check if standard checkout...
                if(Mage::helper('nullor_yeahmobi')->onestepcheckoutEnabled()){ //One Step Checkout
                    if($pageSection != 'checkout_onepage_success'){
                        return;
                    }
                }

                //Unset event
                Mage::getModel('core/session')->unsYeahmobiPurchase();
                
                /*
                $orderId            = Mage::getSingleton('checkout/session')->getLastRealOrderId();
                $order              = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $orderGrandTotal    = number_format($order->getGrandTotal(),2);
                
                return "mkq('track', 'Purchase', {
                    value:          '".$orderGrandTotal."',
                    currency:       '".$currentCurrency."'
                });";
                */

                /*
                $extra = $this->getPurchase();
                $extra = json_encode($extra);
                return "mkq('track', 'Purchase', ". $extra . ");";
                */

                
                $extra = Mage::getModel('core/session')->getData('yeahmobi_purchase');
                Mage::getSingleton('core/session')->unsetData('yeahmobi_purchase');
                $extra = json_encode($extra);
                return "mkq('track', 'Purchase', ". $extra . ");";
                
            }
        }
    }  

    protected function getPurchase()
    {
        $order = Mage::getSingleton('sales/order');
        $order->loadByIncrementId(
            Mage::getSingleton('checkout/session')->getLastRealOrderId()
        );
        $totalData = $order->getData();
        $allitems = $order->getAllVisibleItems();

        $this->orderData['value'] = $totalData['grand_total'];
        $this->orderData['content_ids'] = array();
        foreach ($allitems as $item) {
            $this->orderData['content_ids'][] = $item->getData('product_id');
        }

        $extra = array(
            'content_type' => $this->getContentType(),
            'content_ids' => '['. $this->getPurchaseContentIDs() . ']',
            'value' => $this->getPurchaseValue(),
            'currency' => $this->getCurrency()
        );
        return $extra;
    }

    private $orderData = array();

    
    public function getPurchaseValue() {
        return $this->orderData['value'];
    }

    public function getPurchaseContentIDs() {
        return $this->arryToContentIdString($this->orderData['content_ids']);
    }

    /**************************************************************************/


    /**
     * Return Lead event track
     * @return [type]
     */
    /**
     * lead 在此是 潜在客户 之意，怎么定义潜在客户？
     * 
     */
    public function getLeadEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->leadEnabled()){            
            $pixelEvent = Mage::getModel('core/session')->getYeahmobiLead();
            if($pixelEvent){
                //Unset event
                Mage::getModel('core/session')->unsYeahmobiLead();
                
                return "mkq('track', 'Lead');";
            }
        }
    }


    /**************************************************************************/


    /**
     * Return CompleteRegistration event track
     * @return [type]
     */
    public function getCompleteRegistrationEvent(){
        $pageSection = $this->_getSection();

        //Check if event is enabled
        if(Mage::helper('nullor_yeahmobi')->completeRegistrationEnabled()){
            $pixelEvent = Mage::getModel('core/session')->getYeahmobiCompleteRegistration();
            if($pixelEvent){
                //Unset event
                Mage::getModel('core/session')->unsYeahmobiCompleteRegistration();
                /*
                return "mkq('track', 'CompleteRegistration');";
                */

                $extra = Mage::getModel('core/session')->getData('yeahmobi_completeregistration');
                Mage::getSingleton('core/session')->unsetData('yeahmobi_completeregistration');
                $extra = json_encode($extra);
                return "mkq('track', 'CompleteRegistration', ". $extra . ");";
            }
        }
    }
}